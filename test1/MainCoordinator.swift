//
//  MainCoordinator.swift
//  test1
//
//  Created by Danil Astakhov on 25/11/2019.
//  Copyright © 2019 Danil Astakhov. All rights reserved.
//

import UIKit

class MainCoordinator : ChildCoordinatorProtocol {
    
    
    enum Controller : CoordinatorContrllers {
        case first(_ one: Model1)
        case second(_ two: Model2, _ three: Model3)
        case scroll
    }
   
    static let shared = MainCoordinator()
    private let navigationController: UINavigationController
    
    
    private init() {
        self.navigationController = UINavigationController(nibName: nil, bundle: nil)
    }
    
    func start() -> UIViewController {
        self.navigationController.viewControllers = [firstController(Model1())]
        return self.navigationController
    }
    
    public func show(_ controller: Controller, animated: Bool, showOption: ShowOption = .push) {
        let controller = { () -> UIViewController in
            switch controller {
            case .first(let model):
                return firstController(model)
            case .second(let model2, let model3):
                return secondController(model2, model3)
            case .scroll:
                return scrollController()
            }
        }()
        
        switch showOption {
        case .push:
            self.navigationController.pushViewController(controller, animated: animated)
        case .popover:
            controller.modalPresentationStyle = .popover
            self.navigationController.topViewController?.present(controller, animated: animated, completion: nil)
        case .modally:
            controller.modalPresentationStyle = .overCurrentContext
            self.navigationController.topViewController?.present(controller, animated: animated, completion: nil)
        }
    }
    
    
    private func firstController(_ model: Model1) -> UIViewController {
        let viewModel = ViewModel(model1: model)
        let controller = ViewController(viewModel: viewModel)
        return controller
    }
    
    private func secondController(_ model2: Model2, _ model3: Model3) -> UIViewController {
        let viewModel = ViewModel(model2: model2, model3: model3)
        let controller = ViewController(viewModel: viewModel)
        return controller
    }
    
    private func scrollController() -> UIViewController {
        let controller = ScrollViewController(nibName: "ScrollViewController", bundle: nil)
        let firstView = UIView(frame: .init(x: 0, y: 0, width: 200, height: 200))
        //let secondView = UIView(frame: .init(x: 0, y: 0, width: 300, height: 300))
        
        firstView.backgroundColor = .red
        //secondView.backgroundColor = .blue
        
        controller.insertView(firstView)
        //controller.insertView(secondView)
        
        return controller
    }
    
}
