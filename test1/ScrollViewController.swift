//
//  ScrollViewController.swift
//  test1
//
//  Created by Danil Astakhov on 25/11/2019.
//  Copyright © 2019 Danil Astakhov. All rights reserved.
//

import UIKit

class ScrollViewController: UIViewController {
    
    struct Margins {
        let left : CGFloat
        let top : CGFloat
        let right : CGFloat
        let bottom : CGFloat
        
        static var zero : Margins {
            return .init(left: 0, top: 0, right: 0, bottom: 0)
        }
        
        static func horizonZero(top: CGFloat, bottom: CGFloat) -> Margins {
            return .init(left: 0, top: top, right: 0, bottom: bottom)
        }
        
        static func veticalZero(left: CGFloat, right: CGFloat) -> Margins {
            return .init(left: left, top: 0, right: right, bottom: 0)
        }
    }
    
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var bottomScrollConstraint: NSLayoutConstraint!
    
    private var bottomContentConstraint: NSLayoutConstraint?
    
    private var views: [(UIView, Margins)] = []
    private let placeHolderView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.alwaysBounceHorizontal = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if views.isEmpty {
            insertView(placeHolderView)
        } else {
            views.forEach({ self.updateView($0.0, margins: $0.1) })
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("--- scrollView: \(scrollView.constraints)")
        
        print("--- views: \(views.map({ $0.0.constraints }))")
    }
    
    func insertView(_ targetView: UIView, margins: Margins = .zero) {
        
        if views.contains(where: { $0.0 == targetView }) {
            print("Error: that view already added!")
            return
        }
        
        self.views.append((targetView, margins))
        
        if let _ = self.viewIfLoaded {
            self.updateView(targetView, margins: margins)
        }
    }

    func updateView(_ targetView: UIView, margins: Margins) {
        
        if self.placeHolderView.superview != nil {
            views.removeAll(where: { $0.0 == self.placeHolderView })
            self.placeHolderView.removeFromSuperview()
            self.placeHolderView.removeConstraints(self.placeHolderView.constraints)
        }
        
        targetView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(targetView)
        
        bottomContentConstraint?.isActive = false
        
        if let lastView = views.last(where: { $0.0.superview != nil })?.0  {
            bottomContentConstraint = lastView.bottomAnchor.constraint(equalTo: targetView.topAnchor, constant: margins.top)
        } else {
            bottomContentConstraint = scrollView.topAnchor.constraint(equalTo: targetView.topAnchor, constant: margins.top)
        }
        
        targetView.heightAnchor.constraint(equalToConstant: 100)
        
        let constraints = [
            scrollView.bottomAnchor.constraint(equalTo: targetView.bottomAnchor, constant: margins.bottom),
            scrollView.leftAnchor.constraint(equalTo: targetView.leftAnchor, constant: margins.left),
            scrollView.rightAnchor.constraint(equalTo: targetView.rightAnchor, constant: margins.right),
            bottomContentConstraint!
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    static func basicConstraints(containerView: UIView, targetView: UIView, margins: Margins) -> [NSLayoutConstraint] {
        var constraints : [NSLayoutConstraint] = []
        
        constraints.append(containerView.topAnchor.constraint(equalTo: targetView.topAnchor, constant: margins.top))
        constraints.append(containerView.bottomAnchor.constraint(equalTo: targetView.bottomAnchor, constant: margins.bottom))
        constraints.append(containerView.leftAnchor.constraint(equalTo: targetView.leftAnchor, constant: margins.left))
        constraints.append(containerView.rightAnchor.constraint(equalTo: targetView.rightAnchor, constant: margins.right))
        
        return constraints
    }
}
