//
//  ChildCoordinatorProtocol.swift
//  test1
//
//  Created by Danil Astakhov on 25/11/2019.
//  Copyright © 2019 Danil Astakhov. All rights reserved.
//

import Foundation

protocol ChildCoordinatorProtocol {
    associatedtype Controllers : CoordinatorContrllers
    func show(_ controller: Controllers, animated: Bool, showOption: ShowOption)
}

protocol CoordinatorContrllers { }

enum ShowOption {
    case push
    case popover
    case modally
}
