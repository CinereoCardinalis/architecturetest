//
//  ViewController.swift
//  test1
//
//  Created by Danil Astakhov on 25/11/2019.
//  Copyright © 2019 Danil Astakhov. All rights reserved.
//

import UIKit

class ViewController : UIViewController {
    
    private let viewModel : ViewModel
    
    init(viewModel: ViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "StartView", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @IBAction func showButtonTapped(_ sender: Any?) {
        viewModel.showScroll()
    }
}
