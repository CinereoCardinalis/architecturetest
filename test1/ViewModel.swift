//
//  ViewModel.swift
//  test1
//
//  Created by Danil Astakhov on 25/11/2019.
//  Copyright © 2019 Danil Astakhov. All rights reserved.
//

import Foundation


class ViewModel {
    
    let model1: Model1?
    let model2: Model2?
    let model3: Model3?
    
    
    init(model1: Model1? = nil, model2: Model2? = nil, model3: Model3? = nil) {
        self.model1 = model1
        self.model2 = model2
        self.model3 = model3
    }
    
    
    func showNext1() {
        guard let model1 = model1 else { return }
        MainCoordinator.shared.show(.first(model1), animated: true)
    }
    
    func showNext2() {
        guard let model2 = self.model2,
            let model3 = self.model3
        else { return }
        MainCoordinator.shared.show(.second(model2, model3), animated: true, showOption: .popover)
    }
    
    func showScroll() {
        MainCoordinator.shared.show(.scroll, animated: true, showOption: .push)
    }
}
